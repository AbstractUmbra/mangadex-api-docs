---
label: Public clients
order: 2
icon: people
---

!!!warning
Public clients are not yet available. They will be documented when enabled.

Follow our announcement channels to be the first to know when they are.
!!!
