---
label: Contributing
order: 0
expanded: true
icon: git-pull-request
---

# Contributing

This documentation is a constant effort, and we're more than welcome to
people [contributing to it.](https://gitlab.com/mangadex-pub/mangadex-api-docs)

If you have questions about what to contribute and how, reach out to us in the **#dev-talk-api** channel of
our [Discord](https://discord.gg/mangadex) server.

## Contributing code samples

The code samples are intended to help newcomers with concrete examples.

They are usually provided in Python and JavaScript due to the popularity of these languages and the fact that they are
most common for new developers. For the sake of consistency, the following conventions are in place:

- For **Python**, use the [requests](https://pypi.org/project/requests/) package
- For **JavaScript**, use the [axios](https://www.npmjs.com/package/axios) package (native fetch is acceptable as well)

This will allow the samples to be simpler to parse while focusing on the content rather than the technicalities.
The code examples will be made with the target of allowing you to copy-paste it into your editor or REPL easily.

Our API has full support for editing pages and several resource metadata. Keeping things in order is a heavy enough load
for the site moderators and contributors. Therefore, if you're unsure of whether your program won't break and
inadvertently make changes to thousands of manga due to, say, a recursion issue, **do not make any such attempts to edit
pages.** Make sure your program is bug-free by testing it in a local test environment where you present yourself the
before and after your program's functionality. Any conduct that distorts the site's data en masse will be followed by a
revocation of editing permissions.

### Adding more languages to the code examples

The guides will always have Python and JavaScript code examples to cover the largest percentage of consumers, but we
always welcome people adding more examples in a language they're confident in. Before submitting a merge request, please
make sure that

- Your code is copy-pastable on the terminal if the language has a REPL (for example, an empty line before the indented
  block is expected to close will throw an error in Python's REPL).
- Your code is clean, straightforward, and simple. Remember that this is not to show off your skills as a programmer,
  but help new programmers who might not be the most well-versed in that language. If comprehensibility comes at the
  cost of efficiency, then so it should be. This is not to discourage incorporating efficient solutions or best
  practices, but to state that simplicity should be a higher priority.
- It's not mandatory to cover all samples with a new language, but it should be a goal for any new language to
  eventually have its own sample for all operations currently covered with samples.

All the code examples must be wrapped around a `:::code-block:::` component on the markdown. Your markdown code should
look something like this:

````
:::code-block
```python
print("Hello, World!")
```
:::
````

This makes the code block have a fixed max-height as per the custom CSS, and be scrollable on the inside.

!!!Tip
To provide an interactive example, you might need to set variables/constants like a specific manga's id. Try to keep
them on separate code blocks from the parts where the request is sent and handled. This will help users copy paste the
logic while setting those parameters on their own if they wish to test them on the resources of their choice.
!!!

Try to make the code examples useful in combination with each other. For example, it would be nice if the user can use
the code from the Login example to proceed with using an authenticated resource without wasting time on figuring it out
or writing it on their own. This is achieved simply by using the same variable names across the examples,
like `sessionToken` for example.
