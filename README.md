# MangaDex API Docs

This is the working repository for [https://api.mangadex.org/docs](https://api.mangadex.org/docs).

## Requirements

- NodeJS 18 or higher
- Yarn 1.x (not 2.x)

## Get started

With any recent-enough nodejs installation and the [`yarn CLI`](https://yarnpkg.com) in your path, you can run the
following:

- `yarn`: Installs the dependencies of the project
- `yarn generate`: Generates markdown files from the OpenAPI spec
- `yarn dev`: Starts a local server
- `yarn build`: Generates the static copy of the site in the `.retype` directory

Note: You may use `npm` equivalent commands, but you're on your own, and do not commit your local `package-lock.json` 
file.

### Formatting

Don't reformat things needlessly.

We have a [.editorconfig](.editorconfig) file for the few bits that somewhat matter.

### Docker image

You shouldn't need to worry about it but you can build the site then run the image. It should be browsable at `/docs`.
