---
label: Concepts
order: 4
expanded: true
icon: info
---

Some concepts are broadly used (and useful) when interacting with out API. We recommend you get familiar with them to
save yourself a lot of requests and headaches.
